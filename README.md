# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Fintech Ecosystem Proyect

### How do I get set up? ###

- git clone
- mkvirtualenv fintech
- pip install -r requirements.py
- [in course] sh scripts/init_db.sh
- python manage.py runserver

### Contribution guidelines ###

Flask-sqlalchemy
    - pythonhosted.org/Flask-SQLAlchemy

SQLAlchemy
    - docs.sqlalchemy.org/en/latest

Flask-wtf
    - https://flask-wtf.readthedocs.io/en/stable/

Flask Script
    - http://flask-script.readthedocs.org/en/latest/

Flask Login
	- http://flask-login.readthedocs.io/en/latest/

Moment
    - https://github.com/miguelgrinberg/flask-moment

Select2
    - https://ivaynberg.github.io/select2/

Jinja filters
    - http://jinja.pocoo.org

flask_debugtoolbar
    - https://github.com/mgood/flask-debugtoolbar

Flask-Testing
    - http://pythonhosted.org/Flask-Testing/

nose: test runner
    - https://nose.readthedocs.org/en/latest/

blueprint
    - http://flask.pocoo.org/docs/latest/blueprints/

Appfactories
    - http://flask.pocoo.org/docs/latest/patterns/appfactories/

Config
    - http://flask.pocoo.org/docs/latest/config



### Who do I talk to? ###

- ricardoc@blanclabs.com